
import UIKit

class TableViewController: UITableViewController {
  
    var Articles = [Article]()
    
    var refresher : UIRefreshControl!
    
    let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height:40))
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.downloadJsonWithURL()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        setUpActivityIndicator()
        setUpRefreshing()
    }
    
    //Show loading indicator as user feedback
    func setUpActivityIndicator(){
        indicator.color = UIColor.darkGray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
        indicator.bringSubview(toFront: self.view)
        indicator.startAnimating()
    }
    
    //show refresh control
    func setUpRefreshing(){
        refresher = UIRefreshControl()
        refresher.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refresher.addTarget(self, action: #selector(TableViewController.downloadJsonWithURL), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresher)
    }

    // get json data from url and parsing data
    func downloadJsonWithURL(){
        guard let url = URL(string: "https://api.myjson.com/bins/51mx1") else {
            return
        }
        
        URLSession.shared.dataTask(with: url, completionHandler: {(data, response, error) -> Void in
            
            guard error == nil else {
                print("error happened")
                return
            }
            
            guard data != nil else {
                print("Error: did not receive data")
                return
            }
            
            guard let jsonObj = try? JSONSerialization.jsonObject(with: data!) as? [[String:Any]] else{
                print("error trying to convert data to JSON")
                return
            }
            
            //clear the table view to load new data
            self.Articles.removeAll()
            
            for resultItem  in jsonObj!{
                if let resultDict = resultItem as? NSDictionary{
                    
                    let title: String = {
                        if let title = resultDict.value(forKey: "title") {
                            return title as! String
                        }
                        return "dummy title"
                    }()
                    
                    let abstract: String = {
                        if let abstract = resultDict.value(forKey: "abstract") {
                            return abstract as! String
                        }
                        return "dummy abstract"
                    }()
                    
                    let imageUrl: String = {
                        if let imageUrl = resultDict.value(forKey: "image_url") {
                            return imageUrl as! String
                        }
                        return "dummy imageUrl"
                    }()
                    
                    let url: String = {
                        if let url = resultDict.value(forKey: "url") {
                            return url as! String
                        }
                        return "dummy url"
                    }()
                    
                    self.Articles.append(Article(title : title, abstract : abstract, imageUrl : imageUrl, url : url))
                    
                    // update the table view on main thread
                    OperationQueue.main.addOperation({
                        self.indicator.stopAnimating()
                        self.tableView.reloadData()
                        if (self.refresher.isRefreshing){
                            self.refresher.endRefreshing()
                        }
                        
                        
                    })
                    
                }
                
            }
            
        }).resume()
    }
}

extension TableViewController{
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ArticleTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleTableViewCell
        
        let item = self.Articles[indexPath.row]
        
        cell.titleLabel?.text = item.title
        cell.abstractLabel?.text = item.abstract
        
        let imgURL = NSURL(string: item.imageUrl)
        
        if imgURL != nil {
            if let data = NSData(contentsOf: (imgURL as URL?)!){
                cell.imgView?.image = UIImage(data: data as Data)
            }else{
                // remove photo if image_url is not available and expand the lables
                if let constraint = (cell.imgView?.constraints.filter{$0.firstAttribute == .width}.first) {
                    constraint.constant = 0
                }
                cell.imageView?.removeFromSuperview()
                cell.imgView?.image = nil
                cell.titleLabel?.frame.size.width =  self.view.frame.size.width
                cell.abstractLabel?.frame.size.width =  self.view.frame.size.width
                
            }
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Articles.count
    }
    
    //Open the ‘url’ in an in-app web browser when the cell is tapped
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let item = self.Articles[indexPath.row]
        if(item.url != "dummy url") {
            let url = URL(string: item.url)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}














