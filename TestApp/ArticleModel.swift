import UIKit

// Article Model
class Article {
    
    let title : String
    let abstract : String
    let imageUrl : String
    let url : String
    
    init(title : String, abstract : String, imageUrl : String, url : String) {
        self.title = title
        self.abstract = abstract
        self.imageUrl = imageUrl
        self.url = url
    }
    
}
