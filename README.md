# Project description
Short description about the project structure and workflow :
The main class of the app is TableViewController.swift which handles getting JSON data from url and parsing it. The app has custom table view cells and it’s defined in ArticleTableViewCell.swift.
I defined a model called ArticleModel.swift and it’s used when I want to parse the JSON data and populate the table view. I used autoLayout so The UI adopts to various screen sizes.
